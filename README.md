# MY MDB

The intention of MyMDB is to have an aggregate social platform to browse and post reviews of all kinds of media. MyMDB supports queries for books, movies, games, and music. Users can friend each other and view their friend’s reviews. Users can also search reviews by name and type.

## Technologies Used

 - Java - 1.8
 - Spring Boot & MVC
 - PostgreSQL
 - AWS: RDS, EC2 and S3

## Features

Users can:

 - Register & Log In
 - View thier own profile
 - View other user's profile
 - Create new reviews
 - Search for reviews by reviewer or review title
 - Add other users as friends

To-do list:

 - Remove a user as a friend
 - View user's statistics ie. Number of reviews
 - Refine review searches ie. Search by author, director, etc.

## Usage

## Contributors

Revature 2109 Java/Angular/Microservices Batch:	
 -  @davy-paul	
 -  @eliott.kiefer
 -  @jordanhui920
 -  @miwhitaker	
 -  @risamorishima	
 -  @ZachJZ	

## License
This project uses an MIT license.

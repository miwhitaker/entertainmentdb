import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Registration } from './registration';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  //TODO: add the port and url for server here
  private port:string = "9002";
  // private host:string = "localhost";
  private host:string = "ec2-18-117-181-11.us-east-2.compute.amazonaws.com";
  private url:string = `http://${this.host}:${this.port}/logincontroller/register`;

  constructor(private http: HttpClient) { }

  public insertUser(user:any): Observable<Registration>{
    const httpHeader = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
      }) 
    }; 
    return this.http.post<Registration>(this.url, user, httpHeader);
  }


}

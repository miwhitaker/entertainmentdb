import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { RegistrationService } from './registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  regGroup = new FormGroup({
    userName: new FormControl(''),
    password: new FormControl(''),
    email: new FormControl('')
  })

  constructor(private uSvc: RegistrationService) {
    uSvc = this.uSvc;
   }

  regSuccess:boolean = false

  regUser(reg:FormGroup):void {
    let errMess:any = document.getElementById('errorMessage');
    let newUser = JSON.stringify(reg.value);
    this.uSvc.insertUser(newUser).subscribe(
      (response:any) => {
        if(response) {
          errMess.innerHTML = '';
          let clearForm:any = document.querySelector('.regContainer');
          clearForm.remove();
          this.regSuccess = true;
        }
        else {
          errMess.innerHTML = "This username is already taken, please try a different one";
        }
      }
    ) 
  }

  ngOnInit(): void {
  }

}
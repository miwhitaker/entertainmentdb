import { Component, OnInit } from '@angular/core';
import { Review } from '../review/review';
import { MusicService } from './music.service';

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.css']
})
export class MusicComponent implements OnInit {
  mediaType = 'music';
  reviewList: Review[] = [];
  constructor(private musicServ:MusicService) { }

  ngOnInit(): void 
  {
    this.musicServ.getAllMusicReviews().subscribe(
      response =>{this.reviewList = response;
        this.reviewList.sort((a,b) =>{
          return this.getTime(b.postedDate) - this.getTime(a.postedDate);
        });
      }
    );
  }

  public getSearch(result: Review[]){
    this.reviewList=result
  }

  private getTime(date?: Date) {
    return date != null ? new Date(date).getTime() : 0;
  }

}

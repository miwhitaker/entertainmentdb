import { Component, OnInit, SystemJsNgModuleLoader } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userGroup = new FormGroup({
    userName: new FormControl(''),
    password: new FormControl(''),
  })

  constructor(private modalServ: NgbModal, private logSvc: LoginService, private router:Router) {
    logSvc = this.logSvc;
  }

  loginUser(user:FormGroup):void {
    let currentUser = JSON.stringify(user.value);
    this.logSvc.getUser(currentUser).subscribe(
      (response: any) => {
        if(response) {
          this.router.navigate(['/home']);
          sessionStorage.setItem('user', JSON.stringify(response));
          //to clear session, do sessionStorage.clear()
          //don't open new tabs, might break sessionStorage
        }
        
        else{
          let message:any = document.getElementById("errorMessage");
          message.innerHTML = "Your username and/or password is incorrect";
        }
      }
    )
  }
  
  
  showResetPasswordDiv:boolean = true;
  showSentPasswordToEmail:boolean = false;
  showResetResult:boolean = false;
  failedResetResult:boolean = false;
  successResetResult:boolean = false;
  showModal(content: any) {
    var forgotPasswordModal = this.modalServ.open(content);
    this.showResetPasswordDiv = true;
    this.showSentPasswordToEmail = false;
    this.showResetResult = false;
    this.failedResetResult = false;
    this.successResetResult = false;
    forgotPasswordModal.result.then(()=>{
      //on data
    },()=>{ 
      //on clicking out of modal
    this.showResetPasswordDiv = false;
    });
  }
  sendPasswordCodeForm = new FormGroup({
    email: new FormControl('', Validators.required)
  });
  resetPasswordForm = new FormGroup({
    email: new FormControl('', Validators.required),
    newPassword: new FormControl('', Validators.required),
    passwordCode: new FormControl('', Validators.required)
  });
  sendPasswordCode(sendPasswordCodeForm: FormGroup):void {
    //alert("clicked sendpasswordcode with" + content.value.email);

    this.logSvc.sendPasswordResetCode(JSON.stringify(sendPasswordCodeForm.value));
    this.showResetPasswordDiv = false;
    this.showSentPasswordToEmail = true;
  }
  resetPasword(newPasswordCodeForm: FormGroup):void {
    this.successResetResult = false;
    this.failedResetResult = false;
    this.showResetPasswordDiv = true;
    this.showResetResult = true;
    this.logSvc.updatePassword(JSON.stringify(newPasswordCodeForm.value)).subscribe(
      (response: any) => {
        if (response)
          this.successResetResult = true;
        else
          this.failedResetResult = true;
      }
    );
    //alert("clicked resetpassword" + content.value.email + content.value.newPassword + content.value.passwordCode)
  }


  
  ngOnInit(): void {
    // let sessionObj = sessionStorage.getItem('user');
    // if(sessionObj != null) this.router.navigate(['/home']);
  }

}

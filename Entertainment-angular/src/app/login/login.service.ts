import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Login } from '../login/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //TODO: add the port, host and correct url for the server
  private port:string = "9002";
  // private host:string = "localhost"
  private host:string = "ec2-18-117-181-11.us-east-2.compute.amazonaws.com";
  private url:string = `http://${this.host}:${this.port}/logincontroller/login`;

  constructor(private http: HttpClient) { }

  public getUser(user:any) {
    const httpHeader = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
      }) 
    }; 
    return this.http.post<Login>(this.url, user, httpHeader);
  }

  public sendPasswordResetCode(emailJson:any) {
    const httpHeader = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
      }) 
    };
    //console.log("reached sendPasswordResetCode in login.service.ts");
    return this.http.post<Login>(`http://${this.host}:${this.port}/logincontroller/generatePasswordCode`, emailJson,httpHeader);
  }

  public updatePassword(newPasswordFormJson:any):any {
    const httpHeader = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
      }) 
    };
    //console.log("reached updatePassword in login.service.ts");
    return this.http.post<Boolean>(`http://${this.host}:${this.port}/logincontroller/changePassword`, newPasswordFormJson,httpHeader);
  }
}

import { User } from "../user/user";
import { MediaType } from "./mediaType";

export class Review{
    constructor(public description:string, public mediaTitle:string, public mediaImage:string, public mediaType:MediaType, public postedDate:Date, public rating:number, public user:User){};
}
import { User } from "../user/user";
import { Friend } from "./friend";
import { MediaType } from "./mediaType";

export class FriendRelation{
    //constructor(public friend:Friend, ){}
    constructor(public friend: Friend, public friendId: number, public user: User){};
}
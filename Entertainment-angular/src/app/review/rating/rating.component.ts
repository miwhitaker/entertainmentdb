import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit, OnChanges {

  @Input()
  rating!: number;
  ratingWidth = 0;

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    this.ratingWidth = this.rating*75/5;
  }

  ngOnInit(): void {
  }

}

import { MediaType } from "./mediaType";

export class Media{
    constructor(public title:string, public mediaType:MediaType, public image:string, public mediaId?:number){};
}
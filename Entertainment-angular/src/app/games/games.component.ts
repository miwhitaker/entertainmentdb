import { Component, OnInit } from '@angular/core';
import { Review } from '../review/review';
import { GamesService } from './games.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
  mediaType = 'game';

  reviewList: Review[] = [];
  constructor(private gameServ:GamesService) { }

  ngOnInit(): void 
  {
    this.gameServ.getAllGameReviews().subscribe(
      response =>{
        this.reviewList = response;
        this.reviewList.sort((a,b) =>{
          return this.getTime(b.postedDate) - this.getTime(a.postedDate);
        });
      }
    );
  }

  public getSearch(result: Review[]){
    this.reviewList=result
  }

  private getTime(date?: Date) {
    return date != null ? new Date(date).getTime() : 0;
  }
  
}
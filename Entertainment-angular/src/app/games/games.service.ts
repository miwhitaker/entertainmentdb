import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Review } from '../review/review';

@Injectable({
  providedIn: 'root'
})
export class GamesService {
  
  // private baseURL = "http://localhost:9001/user/"
  private baseURL =  "http://ec2-18-117-181-11.us-east-2.compute.amazonaws.com:9002/user/"

  constructor(private http: HttpClient) { }

  public getAllGameReviews(): Observable<Review[]>
  {
    return this.http.get<Review[]>(this.baseURL+"get_media_type_reviews/game");
  }
}
import { TestBed } from '@angular/core/testing';

import { SearchReviewService } from './search-review.service';

describe('SearchReviewService', () => {
  let service: SearchReviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchReviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Review } from '../review/review';

@Injectable({
  providedIn: 'root'
})
export class SearchReviewService {
  
  // private baseURL = "http://localhost:9001/user/"
  
  private baseURL =  "http://ec2-18-117-181-11.us-east-2.compute.amazonaws.com:9002/user/"

  constructor(private http: HttpClient) { }

  public getReviewsBySearch(pathVars: Map<string, string>): Observable<Review[]>{
    const httpHead = {
      headers: new HttpHeaders({
        "Content-Type":"application/json",
        "Access-Control-Allow-Origin":"*"
      })
    }
    return this.http.post<Review[]>(this.baseURL+pathVars.get("mediaType")+"/reviews/"+pathVars.get("searchText"), httpHead);
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Review } from '../review/review';
import { SearchReviewService } from './search-review.service';

@Component({
  selector: 'app-search-review',
  templateUrl: './search-review.component.html',
  styleUrls: ['./search-review.component.css']
})
export class SearchReviewComponent implements OnInit {

  constructor(private modalServ: NgbModal, private searchServ: SearchReviewService) { }

  ngOnInit(): void {
  }

  @Input()
  mediaType!: string;
  @Output()
  searchResult = new EventEmitter<Review[]>();
  

  pathVars: Map<string, string> = new Map<string, string>();
  reviewList: Review[] = [];
  searchGroup = new FormGroup({
    searchText: new FormControl('', Validators.required)
  });
  noSearchFound = false;
  searchText = "";

  public search(search: FormGroup){
    this.searchText = search.value.searchText;
    this.pathVars.set("mediaType", this.mediaType);
    this.pathVars.set("searchText", this.searchText);
    this.searchServ.getReviewsBySearch(this.pathVars).subscribe(
      response =>{
        if(response == null){
          this.noSearchFound = true;
          this.searchResult.emit([]);
        }
        else{
          this.noSearchFound = false;
          this.reviewList = response;
          this.searchResult.emit(this.reviewList);
        }
    });
  }

  showModal(content: any) {
    this.modalServ.open(content, {ariaLabelledBy: 'modal-basic-title'}).result;
  }

}

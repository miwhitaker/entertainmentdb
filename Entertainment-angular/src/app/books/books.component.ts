import { Component, OnInit } from '@angular/core';
import { Review } from '../review/review';
import { BooksService } from './books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  mediaType = 'book';

  reviewList: Review[] = [];
  constructor(private bookServ:BooksService) { }

  ngOnInit(): void 
  {
    this.bookServ.getAllBookReviews().subscribe(
      response =>{
        this.reviewList = response;
        this.reviewList.sort((a,b) =>{
          return this.getTime(b.postedDate) - this.getTime(a.postedDate);
        });
      }
    );
  }

  public getSearch(result: Review[]){
    this.reviewList=result
  }

  private getTime(date?: Date) {
    return date != null ? new Date(date).getTime() : 0;
  }
  
}

import { Component, OnInit } from '@angular/core';
import { Review } from '../review/review';
import { MoviesService } from './movies.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  mediaType = 'movie';
  reviewList: Review[] = [];
  reviewGroup = new FormGroup({
    mediaTitle: new FormControl('', Validators.required),
    mediaImage: new FormControl('', Validators.required),
    mediaType: new FormControl('', Validators.required),
    rating: new FormControl('', [Validators.required, Validators.min(1), Validators.max(5)]),
    postedDate: new FormControl(''),
    description: new FormControl('', Validators.required),
    imagePicker: new FormControl(true, Validators.required)
  });

  constructor(private moviesServ:MoviesService) { }

  ngOnInit(): void 
  {
    this.moviesServ.getAllMovieReviews().subscribe(
      response =>{
        this.reviewList = response;
        this.reviewList.sort((a,b) =>{
          return this.getTime(b.postedDate) - this.getTime(a.postedDate);
        });
        }
    );
  }

  public getSearch(result: Review[]){
    this.reviewList=result
  }

  private getTime(date?: Date) {
    return date != null ? new Date(date).getTime() : 0;
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Friend } from '../review/friend';
import { FriendRelation } from '../review/friend-relation';
import { Review } from '../review/review';
import { User } from './user';

@Injectable({
    providedIn: 'root'
})
export class UserService{

    // private baseUserURL = "http://localhost:9001/user/"
    private baseUserURL =  "http://ec2-18-117-181-11.us-east-2.compute.amazonaws.com:9002/user/"

    constructor(private http: HttpClient) { }

    userList = [];

    public getFriends(username:string): Observable<FriendRelation[]>{
        return this.http.get<FriendRelation[]>(this.baseUserURL+username+"/get_friends_by_username");
    }

    public selectAllByUserFriendId(userId:number): Observable<Review[]>{
        return this.http.get<Review[]>(this.baseUserURL+userId+"/get_my_freind_reviews");
    }

    public selectThisUsersReviews(userId:number): Observable<Review[]>{
        return this.http.get<Review[]>(this.baseUserURL+userId+"/get_this_user_reviews");
    }

    public getUsersReviews(username:string):Observable<Review[]>{
        return this.http.get<Review[]>(this.baseUserURL+"/get_reviews/"+username);
    }

}
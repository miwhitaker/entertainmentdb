

/*
    an exported class
    the component decorator
    the location of its css and html files
    the selector we will use to reference the component and its view
*/

import { Component, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AppComponent } from "../app.component";
import { HomeComponent } from "../home/home.component";
import { HomeService } from "../home/home.service";
import { profilePicture } from "../home/profilepicture";
import { Friend } from "../review/friend";
import { FriendRelation } from "../review/friend-relation";
import { Review } from "../review/review";
import { User } from "./user";
import { UserService } from "./user.service";

@Component({
    selector: 'app-user',
    templateUrl : './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent{

    currentUser: any;

    pageUsername: any = "none";
    isActiveUserPage = false;
    isFollowing = false;

    friendList: FriendRelation[] = [];

    reviewList: Review[] = []; 

    @ViewChild('validInput') validInputModal: any;
    noFriends = false;
    canPost = true;

    userList: string[] = []; 
    profilePic: any = "data:image;base64, ";

    constructor(private route: ActivatedRoute, private modalServ: NgbModal, private router: Router, private userServ: UserService, private homeServ:HomeService, private app:AppComponent, private sanitizer:DomSanitizer) { }
    
    ngOnInit () : void{
      this.app.displayButton();
      this.pageUsername = this.route.snapshot.paramMap.get('pageUser');
      let sessionObj = sessionStorage.getItem('user');

      if(sessionObj == null){
        this.router.navigate(['/login']);
      }else{
        this.currentUser = JSON.parse(sessionObj|| '{}')  
        //console.log(this.currentUser);
        if (this.currentUser.username == this.pageUsername)  {
          this.getProfilePicture(this.currentUser.username);
          //console.log("the logged in user and the page user are the same")
          //my userpage logic
          this.isActiveUserPage = true;
          //get my friends reviews
          this.userServ.getFriends(this.currentUser.username).subscribe(
            response =>{
              if(response == null)
                this.noFriends = true;
              else{ 
                this.noFriends = false;
                this.friendList = response;
                this.friendList.sort((a, b) => {
                  if (a > b) {return 1;
                  }else if (a < b) {return -1;
                  }else{return 0;
                  }
                });
              }
          });
          //this.userServ.selectAllByUserFriendId
          this.userServ.selectAllByUserFriendId(this.currentUser.userId).subscribe(
            response =>{
              if(response == null)
                this.noFriends = true;
              else{
                this.noFriends = false;
                this.reviewList = response;
                this.reviewList.sort((a,b) =>{
                  return this.getTime(b.postedDate) - this.getTime(a.postedDate);
                });
              }
          });
        }
        else{
          //console.log("the logged in user and the page user are NOT the same")
          this.isActiveUserPage = false;
          //console.log(this.pageUsername);
          //others' user page logic
          this.getProfilePicture(this.pageUsername);
          this.userServ.getFriends(this.pageUsername).subscribe(
            response =>{
              if(response == null)
                this.noFriends = true;
              else{ 
                this.noFriends = false;
                this.friendList = response;
                this.friendList.sort((a, b) => {
                  if (a > b) {return 1;
                  }else if (a < b) {return -1;
                  }else{return 0;
                  }
                });
              }
          });
          this.userServ.getUsersReviews(this.pageUsername).subscribe(
            response =>{
              if(response == null)
                this.noFriends = true;
              else{
                this.noFriends = false;
                this.reviewList = response;
                this.reviewList.sort((a,b) =>{
                  return this.getTime(b.postedDate) - this.getTime(a.postedDate);
                });
              }
          });
        }
      }
    }


    private getTime(date?: Date) {
      return date != null ? new Date(date).getTime() : 0;
    }

    toggleFollowUser(){
        if (this.isFollowing){
            this.isFollowing = false;

        }
        else if (!this.isFollowing){
            this.isFollowing = true;
        }
    }

    checkCurrentUser(){
        
    }

    displayFollowing(){

    }

    public getProfilePicture(username:string):any
  {
    this.homeServ.getProfilePicture(username).subscribe(
      response => {
        this.profilePic += response[0].image;
        this.profilePic = this.sanitizer.bypassSecurityTrustUrl(this.profilePic);
        let pic:profilePicture[];
        pic = response
        //console.log(this.sanitizer.bypassSecurityTrustUrl(pic[0].image));
        //console.log(response[0].image);
        return this.profilePic;
      }
    );
  }
}

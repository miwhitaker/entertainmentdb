import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(arrays: any[], text: any): any[] {
    if(!arrays)  return [];
    if(!text) return [];
    return arrays.filter(element =>{
      return element.toLowerCase().includes(text.toLowerCase());
    });
  }
}

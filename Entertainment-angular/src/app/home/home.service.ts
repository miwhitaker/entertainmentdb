import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Media } from '../review/media';
import { Review } from '../review/review';
import { User } from '../user/user';
import { profilePicture } from './profilepicture';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  // private baseUserURL = "http://localhost:9001/user/"
  // private baseMediaURL = "http://localhost:9001/media_search/"

  private baseUserURL =  "http://ec2-18-117-181-11.us-east-2.compute.amazonaws.com:9002/user/"
  private baseMediaURL =  "http://ec2-18-117-181-11.us-east-2.compute.amazonaws.com:9002/media_search/"

  constructor(private http: HttpClient) { }

  public getAllUsers(userId:number): Observable<User[]>{
    return this.http.get<User[]>(this.baseUserURL+userId+"/get_all");
  }

  public insertFriend(userId:number, friend: String): Observable<String>{
    const httpHead = {
      headers: new HttpHeaders({
        "Content-Type":"application/json",
        "Access-Control-Allow-Origin":"*"
      })
    }
      return this.http.post<String>(this.baseUserURL+userId+"/add_friend", friend, httpHead);
  }

  public getTimelineReviews(userId:number): Observable<Review[]>{
    return this.http.get<Review[]>(this.baseUserURL+userId+"/get_timeline");
  }

  public insertReview(userId:number, review: any): Observable<Review>{
    const httpHead = {
      headers: new HttpHeaders({
        "Content-Type":"application/json",
        "Access-Control-Allow-Origin":"*"
      })
    }
      return this.http.post<Review>(this.baseUserURL+userId+"/post_review", review, httpHead);
  }

  public getReviewsBySearch(searchText: string): Observable<Review[]>{
    const httpHead = {
      headers: new HttpHeaders({
        "Content-Type":"application/json",
        "Access-Control-Allow-Origin":"*"
      })
    }
    return this.http.post<Review[]>(this.baseUserURL+"reviews/"+searchText, searchText, httpHead);
  }

  public getImage(type: string, title: string):Observable<Media[]>{
    return this.http.get<Media[]>(this.baseMediaURL+type+"/"+title);
  }

  public submitImage(file: FormData, id:number)
  {
    const httpHead = {
      headers: new HttpHeaders({
        //"Content-Type":"multipart/form-data",
        "Access-Control-Allow-Origin":"*"
      })
    }
    window.location.reload;
    return this.http.post<any[]>(this.baseUserURL+id+"/submit_picture", file, httpHead);
  }

  public getProfilePicture(username:string):Observable<profilePicture[]>
  {
    return this.http.get<profilePicture[]>(this.baseUserURL+username+"/get_picture");
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Review } from '../review/review';
import { HomeService } from './home.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Media } from '../review/media';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { DomSanitizer } from '@angular/platform-browser';
import { profilePicture } from './profilepicture';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser: any;

  reviewList: Review[] = [];
  reviewGroup = new FormGroup({
    mediaTitle: new FormControl('', Validators.required),
    mediaImage: new FormControl('', Validators.required),
    mediaType: new FormControl('', Validators.required),
    rating: new FormControl('', [Validators.required, Validators.min(1), Validators.max(5)]),
    postedDate: new FormControl(''),
    description: new FormControl('', Validators.required),
    imagePicker: new FormControl(true, Validators.required)
  });

  @ViewChild('validInputModal') validInputModal: any;
  noFriends = false;
  canPost = true;

  friendSearchText = '';
  userList: string[] = [];
  userGroup = new FormGroup({
    username: new FormControl('', Validators.required)
  });
  @ViewChild('addFriendModal') addFriendModal: any;
  notChosen = false;
  friendAdded = false;

  searchGroup = new FormGroup({
    searchText: new FormControl('', Validators.required)
  });

  profilePicture = new FormGroup({
    ppFile: new FormControl('')
  });

  selectedFile:any;

  profilePic:any = "data:image;base64, ";

  noSearchFound = false;
  searchText = "";

  mediaList: Media[] = [];
  @ViewChild('chooseImageModal') chooseImageModal:any;

  constructor(private modalServ: NgbModal, private router: Router, private homeServ: HomeService, private app:AppComponent, private sanitizer:DomSanitizer) { }

  ngOnInit(): void {
    this.app.displayButton();
    let sessionObj = sessionStorage.getItem('user');
    if(sessionObj == null){
      this.router.navigate(['/login']);
    }else{
      this.currentUser = JSON.parse(sessionObj|| '{}')
      
      this.homeServ.getTimelineReviews(this.currentUser.userId).subscribe(
      response =>{
        //console.log(response);
        if(response == null)
          this.noFriends = true;
        else{
          this.noFriends = false;
          this.reviewList = response;
          this.reviewList.sort((a,b) =>{
            return this.getTime(b.postedDate) - this.getTime(a.postedDate);
          });
        }
    });
    this.homeServ.getAllUsers(this.currentUser.userId).subscribe(
      response =>{
        if(response != null)
          response.forEach(user => this.userList.push(user.username));
    });
    }
  }

  showModal(content: any) {
    this.modalServ.open(content, {ariaLabelledBy: 'modal-basic-title'}).result;
  }

  submitImage(files: FormGroup)
  {
    const uploadData = new FormData();
    uploadData.append("uploadFile", this.selectedFile);
    this.homeServ.submitImage(uploadData, this.currentUser.userId).subscribe(
      response =>{
        console.log(response);
        //let receivedImage:any = response;
        //let base64Data:any = response;
        //let convertedImage = "data:image/jpeg;base64," + base64Data; 
      }, error =>{
        //window.location.reload();
        this.modalServ.dismissAll();
      });
  }

  readFile(event:any)
  {
    this.selectedFile = event.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
  }

  public addFriend(user: FormGroup){
    let chosenUsers:string[] = user.value.username;
    if(chosenUsers.length > 0){
      this.notChosen = false;
      chosenUsers.forEach(username =>{
        this.homeServ.insertFriend(this.currentUser.userId, username).subscribe(
          response =>{
            if(response == null){
              this.showModal(this.addFriendModal);
            }else{
            console.log("Friend Added");
            this.homeServ.getTimelineReviews(this.currentUser.userId).subscribe(
              response =>{
                this.friendAdded = true;
                this.showModal(this.addFriendModal);
                if(response == null)
                  this.noFriends = true;
                else{
                  this.noFriends = false;
                  this.reviewList = response;          
                  this.reviewList.sort((a,b) =>{
                    return this.getTime(b.postedDate) - this.getTime(a.postedDate);
                  });
                }
            });
            this.friendSearchText = "";
            this.friendAdded = false;
            this.modalServ.dismissAll();    
          }});
      });
    }else{
      this.notChosen = true;
      this.showModal(this.addFriendModal);
    }   
    this.userGroup = new FormGroup({
      username: new FormControl('', Validators.required)
    });
  }

  public chooseImage(review: FormGroup){
    if(this.validPost(review)){
      if(review.value.imagePicker){
        this.homeServ.getImage(review.value.mediaType.toLowerCase(), review.value.mediaTitle).subscribe(
          response =>{
            if(response != null && response.length > 0){
              response = response.filter(media => media.image.length > 1);
              this.mediaList = response;
              if(this.mediaList.length > 0)
                this.showModal(this.chooseImageModal);
              else
                this.submitReview(review, "");
            }else{
              this.submitReview(review, "");
            }
        });
      }else{
        this.submitReview(review, "");
      }
    } 
  }

  public submitReview(review: FormGroup, image:string){
    this.modalServ.dismissAll();
    review.value.postedDate = new Date();
    review.value.mediaImage = image;
    let reviewJSON = JSON.stringify(review.value);
    this.homeServ.insertReview(this.currentUser.userId, reviewJSON).subscribe(
      response =>{
        console.log("Response Submitted");
        this.reviewList.push(response);
        this.modalServ.dismissAll();
        this.reviewGroup = new FormGroup({
          mediaTitle: new FormControl('', Validators.required),
          mediaImage: new FormControl('get from api', Validators.required),
          mediaType: new FormControl('', Validators.required),
          rating: new FormControl('', [Validators.required, Validators.min(1), Validators.max(5)]),
          postedDate: new FormControl(''),
          description: new FormControl('', Validators.required),
          imagePicker: new FormControl(true, Validators.required)
      });
    },
      error =>{
      console.log("Error:"+error);
    });
          
    this.canPost = true;
    this.showModal(this.validInputModal);

    this.homeServ.getTimelineReviews(this.currentUser.userId).subscribe(
      response =>{
        if(response == null)
          this.noFriends = true;
        else{
          this.noFriends = false;
          this.reviewList = response;
          this.reviewList.sort((a,b) =>{
            return this.getTime(b.postedDate) - this.getTime(a.postedDate);
          });
        }
    });
  }

  public search(search: FormGroup){
    this.searchText = search.value.searchText;
    this.homeServ.getReviewsBySearch(this.searchText).subscribe(
      response =>{
        if(response == null)
          this.noSearchFound = true;
        else{
          this.noSearchFound = false;
          this.reviewList = response;
          this.reviewList.sort((a,b) =>{
            return this.getTime(b.postedDate) - this.getTime(a.postedDate);
          });
        }
    });
  }

  private validPost(review: FormGroup):boolean{
    if(review.get('mediaTitle')?.invalid || review.get('mediaType')?.invalid || review.get('rating')?.invalid || review.get('description')?.invalid){
      this.canPost = false;
      this.showModal(this.validInputModal);
      return false;
    }
    return true;
  }

  private getTime(date?: Date) {
    return date != null ? new Date(date).getTime() : 0;
  }

  public getProfilePicture(username:string):any
  {
    this.homeServ.getProfilePicture(username).subscribe(
      response => {
        this.profilePic += response[0].image;
        this.profilePic = this.sanitizer.bypassSecurityTrustUrl(this.profilePic);
        let pic:profilePicture[];
        pic = response
        //console.log(this.sanitizer.bypassSecurityTrustUrl(pic[0].image));
        //console.log(response[0].image);
        return this.profilePic;
      }
    );
  }

}
import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@Injectable({providedIn: "root"})
export class AppComponent {
  
  title = 'Entertainment-angular';

  currentUser: any;

  ngOnInit(): void{
    let sessionObj = sessionStorage.getItem('user');
    this.currentUser = JSON.parse(sessionObj|| '{}')  
  }

  constructor(private router:Router) {};

    isVisible:boolean = false;
    displayButton():void {
      let session:any = sessionStorage.getItem('user')
      if(session) {
        this.isVisible = true;
      }
    }

    logOutUser() {
      sessionStorage.clear();
      this.router.navigate(['/login']);
      this.isVisible = false;
    }
} 

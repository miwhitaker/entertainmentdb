import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { BooksComponent } from './books/books.component';
import { LoginComponent } from './login/login.component';
import { MoviesComponent } from './movies/movies.component';
import { MusicComponent } from './music/music.component';
import { RegistrationComponent } from './registration/registration.component';
import { GamesComponent } from './games/games.component';
import { FilterPipe } from './filter.pipe';
import { RatingComponent } from './review/rating/rating.component';
import { UserComponent } from './user/user.component';
import { SearchReviewComponent } from './search-review/search-review.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BooksComponent,
    LoginComponent,
    MoviesComponent,
    MusicComponent,
    RegistrationComponent,
    GamesComponent,
    FilterPipe,
    RatingComponent,
    UserComponent,
    SearchReviewComponent
  ],
  imports: [
    BrowserModule, ReactiveFormsModule, HttpClientModule, FormsModule, RouterModule.forRoot([
      {path: "home", component: HomeComponent},
      {path: "login", component: LoginComponent},
      {path: "registration", component: RegistrationComponent},
      {path: "books", component: BooksComponent},
      {path: "music", component: MusicComponent},
      {path: "movies", component: MoviesComponent},
      {path: "user/:pageUser", component: UserComponent},
      {path: "games", component: GamesComponent},
      {path: "**", redirectTo: "login"}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

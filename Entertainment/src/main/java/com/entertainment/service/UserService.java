package com.entertainment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entertainment.model.User;
import com.entertainment.repository.UserRepository;

@Service("userService")
public class UserService {
	private UserRepository uRepo;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public UserService(UserRepository uRepo) {
		super();
		this.uRepo = uRepo;
	}
	
	public User getUserByUsername(String username) {
		User user = uRepo.selectByUsername(username);
		if(user == null) return null;
		return user;
	}
	
	public User getUserById(int id) {
		User user = uRepo.selectById(id);
		if(user == null) return null;
		return user;
	}
	
	public List<User> getAllExceptCurrent(User user){
		List<User> users = uRepo.selectAll();
		if(users.size() == 0) return null;
		users.removeIf((User u) -> u.getUserId() == user.getUserId());
		return users;
	}
}

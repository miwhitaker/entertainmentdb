package com.entertainment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entertainment.model.Friend;
import com.entertainment.model.User;
import com.entertainment.repository.FriendRepository;

@Service("friendService")
public class FriendService {
	
	private FriendRepository fRepo;
	
	public FriendService() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public FriendService(FriendRepository fRepo) {
		super();
		this.fRepo = fRepo;
	}
	
	public List<Friend> getAllFriendByUser(User user){
		List<Friend> friends = fRepo.selectAllByUser(user);
		if(friends.size()==0) return null;
		return friends;
	}
	
	public boolean isFriend(User user, User friend) {
		List<Friend> friends = fRepo.selectAllByUser(user);
		for(Friend f: friends) {
			if(f.getFriend().getUserId() == friend.getUserId())
				return true;
		}
		return false;
	}
	
	public void insertFriend(Friend friend) {
		fRepo.insert(friend);
	}
}

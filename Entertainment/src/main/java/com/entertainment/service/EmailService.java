package com.entertainment.service;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("emailService")
public class EmailService {
	
	@Value("${spring.mail.username}")
	private String emailUsername;

	@Value("${spring.mail.password}")
	private String emailPassword;

	@Value("${spring.mail.host}")
	private String host;

	@Value("${spring.mail.port}")
	private int port;

	@Value("${spring.mail.properties.mail.smtp.auth}")
	private boolean smtpauth;

	@Value("${spring.mail.properties.mail.starttls.enable}")
	private boolean starttls;
	
	Properties p;

	@Autowired
	public EmailService(Properties p) {
		this.p = p;
	}
	
	public void sendEmail(String recipient, String subject, String emailBody) {
		this.p.put("mail.smtp.host", host);
		this.p.put("mail.smtp.port", port);
		this.p.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
		this.p.put("mail.smtp.auth", smtpauth);
		this.p.put("mail.smtp.starttls.enable", starttls);
		this.p.put("mail.smtp.user", emailUsername);
		this.p.put("mail.smtp.password", emailPassword);
        Session session = Session.getInstance(p, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailUsername, emailPassword);
            }
        });
        
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailUsername));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            message.setSubject(subject);
            message.setText(emailBody);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, emailUsername, emailPassword);
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
	}
}

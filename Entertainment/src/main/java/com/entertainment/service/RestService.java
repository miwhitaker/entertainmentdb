package com.entertainment.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service("restService")
public class RestService {
    private RestTemplate restTemplate;

    public RestService() {
		super();
	}
    
	public RestService(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}

	@Autowired
	public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getPostsPlainJSON(String url) {
    	HttpHeaders headers = new HttpHeaders();
    	//must set headers for some apis
    	headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
    	headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36");
    	HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
    	ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
    	return response.getBody();
    }
}

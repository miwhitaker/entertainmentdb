package com.entertainment.service;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.entertainment.model.Media;
import com.entertainment.model.MediaType;

@Service("mediaService")
public class MediaService {
	private RestService rServ;
	
	@Value("${api-keys.movie}")
	private String movieApiKey;

	@Value("${api-keys.music}")
	private String musicApiKey;

	@Value("${api-keys.game}")
	private String gameApiKey;

	@Value("${api-keys.book}")
	private String bookApiKey;

	public MediaService() {
		super();
	}

	@Autowired
	public MediaService(RestService rServ) {
		super();
		this.rServ = rServ;
	}
	
	// please don't execute the functions below too much (like executing everytime a user types the next letter in)
	// results should be properly cached so we don't call the api too much but caching is out of scope of this project imo
	
	public List<Media> searchMovie(String mediaTitle) {
		//only searches movies, only searches page 1
		//documentation https://developers.themoviedb.org/3/search/search-movies
		// November 2, 2021
		List<Media> matchingMediaList = new LinkedList<>();
		String api = "https://api.themoviedb.org/3/search/movie?api_key=";
		api += movieApiKey;
		api += "&language=en-US&page=1&include_adult=false&query=";
		api += mediaTitle;
		String rawJson = rServ.getPostsPlainJSON(api);
		JSONObject returnedJson = new JSONObject(rawJson);
		JSONArray matchingResults = returnedJson.getJSONArray("results"); //results of search in array of JSONObjects
		String baseImageUrl = "https://image.tmdb.org/t/p/w500/"; //w500 can be changed for different sizes
		// a little more information for size can be found here https://developers.themoviedb.org/3/getting-started/images
		for (int i = 0; i < matchingResults.length(); i++) {
			String posterPath = matchingResults.getJSONObject(i).optString("poster_path",""); //optstring=getstring will return 2nd arg if returned null
			String imgUrl = posterPath.length() >= 1 ? baseImageUrl + posterPath : ""; //if no poster, then set imgUrl to empty
			String title = matchingResults.getJSONObject(i).getString("original_title");
			Media newMatchingMedia = new Media(title,new MediaType("movie"),imgUrl);
			matchingMediaList.add(newMatchingMedia);
		}
		return matchingMediaList;
	}
	
	public List<Media> searchMusic(String mediaTitle) {
		//only searches for albums, only searches first page
		// documentation https://www.last.fm/api/intro
		// November 2, 2021
		List<Media> matchingMediaList = new LinkedList<>();
		String api = "http://ws.audioscrobbler.com/2.0/?method=album.search&api_key=";
		api += musicApiKey;
		api += "&format=json&album=";
		api += mediaTitle;
		String rawJson = rServ.getPostsPlainJSON(api);
		JSONObject returnedJson = new JSONObject(rawJson);
		JSONArray matchingResults;
		try {
			//results of search in array of JSONObjects
			matchingResults = returnedJson.getJSONObject("results").getJSONObject("albummatches").getJSONArray("album"); 
		}
		catch (Exception e) {
			e.printStackTrace();
			return matchingMediaList; // throw if no match?
		}
		// a little more information for size can be found here https://developers.themoviedb.org/3/getting-started/images
		Set<String> uniqueTitles = new HashSet<>();
		for (int i = 0; i < matchingResults.length(); i++) {
			String title = matchingResults.getJSONObject(i).getString("name");
			if (uniqueTitles.contains(title)) 
				continue;
			uniqueTitles.add(title);
			JSONArray images = matchingResults.getJSONObject(i).getJSONArray("image");
			//if no images, returns an empty array
			//images now has a few candidates for images depending on size need. just grabbing the first one which is "small" size
			//grabbing 3 now which returns extra large album cover
			String imgUrl = images.getJSONObject(3).optString("#text",""); //optstring=getstring will return 2nd arg if returned null
			Media newMatchingMedia = new Media(title,new MediaType("music"),imgUrl);
			matchingMediaList.add(newMatchingMedia);
		}
		return matchingMediaList;
	}
	
	public List<Media> searchGame(String mediaTitle) {
		//only searches first page
		// documentation https://www.giantbomb.com/api/documentation/#toc-0-41
		// November 2, 2021
		List<Media> matchingMediaList = new LinkedList<>();
		String api = "https://www.giantbomb.com/api/search/?resource_type=game&api_key=";
		api += gameApiKey;
		api += "&format=json&resources=game&query=";
		api += mediaTitle;
		String rawJson = rServ.getPostsPlainJSON(api);
		JSONObject returnedJson = new JSONObject(rawJson);
		JSONArray matchingResults = returnedJson.getJSONArray("results"); 
		for (int i = 0; i < matchingResults.length(); i++) {
			String title = matchingResults.getJSONObject(i).getString("name");
			JSONObject images = matchingResults.getJSONObject(i).getJSONObject("image");
			// images now have a few candidates for images. just using "original_url"
			String imgUrl = images.optString("original_url",""); //optstring=getstring will return 2nd arg if returned null
			Media newMatchingMedia = new Media(title,new MediaType("game"),imgUrl);
			matchingMediaList.add(newMatchingMedia);
		}
		return matchingMediaList;
	}
	
	public List<Media> searchBook(String mediaTitle) {
		// documentation https://openlibrary.org/developers/api
		// November 2, 2021
		List<Media> matchingMediaList = new LinkedList<>();
		String api = "https://openlibrary.org/search.json?title=";
		api += mediaTitle;
		String rawJson = rServ.getPostsPlainJSON(api);
		JSONObject returnedJson = new JSONObject(rawJson);
		JSONArray matchingResults = returnedJson.getJSONArray("docs"); 
		for (int i = 0; i < matchingResults.length(); i++) {
			String title = matchingResults.getJSONObject(i).getString("title");
			int cover_image = -1;
			String isbn = "";
//			try {
//				cover_image = matchingResults.getJSONObject(i).getInt("cover_i"); //doesn't always exist
//			}
//			catch (Exception e) {
//			}
			try {
				isbn = matchingResults.getJSONObject(i).getJSONArray("isbn").getString(0); //doesn't always exist, just get the first isbn
			}
			catch (Exception e) {
			}
			
			String imgUrl = "";
			// note, even if cover_image was returned, it's not guaranteed that a cover_image image will exist
//			if (cover_image >= 0) {
//				imgUrl = "https://covers.openlibrary.org/b/cover/" + cover_image + "-L.jpg?default=false";
//			}
//			else 
				if (isbn.length() > 0) {
				imgUrl = "https://covers.openlibrary.org/b/isbn/" + isbn + "-L.jpg?default=false";
			}
			Media newMatchingMedia = new Media(title,new MediaType("book"),imgUrl);
			matchingMediaList.add(newMatchingMedia);
		}
		return matchingMediaList;
	}
}

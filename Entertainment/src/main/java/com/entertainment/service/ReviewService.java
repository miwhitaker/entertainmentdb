package com.entertainment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entertainment.model.Review;
import com.entertainment.repository.ReviewRepository;

@Transactional
@Service("reviewService")
public class ReviewService {
	
	private ReviewRepository rRepo;
	
	public ReviewService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ReviewService(ReviewRepository rRepo) {
		super();
		this.rRepo = rRepo;
	}

	public List<Review> getReviewsByUserId(int id) {
		List<Review> reviews = rRepo.selectAllByUserId(id);
		if(reviews.size() == 0) return null;
		return reviews;
	}
	
	public List<Review> getReviewsByUserUsername(String name) {
		List<Review> reviews = rRepo.selectAllByUsername(name);
		if(reviews.size() == 0) return null;
		return reviews;
	}
	
	public List<Review> getReviewsByFriendId(int id) {
		List<Review> reviews = rRepo.selectAllByFriendId(id);
		if(reviews.size() == 0) return null;
		return reviews;
	}
	
	public List<Review> getReviewsBySearch(String searchText){
		List<Review> reviews = rRepo.selectAllBySearchText(searchText);
		if(reviews.size() == 0) return null;
		return reviews;
	}
	
	public List<Review> getReviewsBySearchByMediaType(String searchText, String mediaType){
		List<Review> reviews = rRepo.selectAllBySearchTextByMedia(searchText, mediaType);
		if(reviews.size() == 0) return null;
		return reviews;
	}
	
	public void addReview(Review review) {
		rRepo.insert(review);
	}
	
	public List<Review> getReviewsByMediaType(String mediaType)
	{
		List<Review> reviews = rRepo.selectAllByMediaType(mediaType);
		if(reviews.size() == 0) return null;
		return reviews;
	}
	
	//Blame Zach
	public List<Review> getReviewsByFriends(int id){
		List<Review> reviews = rRepo.selectAllByUserFriendId(id);
		if(reviews.size() == 0) {
			return null;
		} 
		return reviews;
	}

}

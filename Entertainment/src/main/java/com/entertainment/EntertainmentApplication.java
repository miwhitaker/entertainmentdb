package com.entertainment;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;


@SpringBootApplication
@ImportResource({"file:src/main/webapp/WEB-INF/applicationContext.xml"})
public class EntertainmentApplication {
	
	public static final Logger log = Logger.getLogger(EntertainmentApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(EntertainmentApplication.class, args);
		log.setLevel(Level.DEBUG);
	}
}

package com.entertainment.controller;

import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entertainment.model.User;
import com.entertainment.repository.UserRepository;
import com.entertainment.service.EmailService;


@RestController
@RequestMapping(path = "/logincontroller")
@CrossOrigin(origins="*")
public class LoginController {
	
	private UserRepository uRepo;
	private EmailService eServ;
	
	public LoginController() {}

	@Autowired
	public LoginController(UserRepository uRepo, EmailService eServ) {
		super();
		this.uRepo = uRepo;
		this.eServ = eServ;
	}
	
	@PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
	public User checkUser(@RequestBody LinkedHashMap<String, String> userMap) {
		User currentUser = uRepo.selectByUsername(userMap.get("userName"));
		if(currentUser == null) {
			return null;
		}
		else if(currentUser.getPassword().equals(sha256(userMap.get("password")))) {
			return currentUser;
		}
		else {
			return null;
		}
	}
	
	@PostMapping(value = "/register", consumes = "application/json", produces = "application/json")
	public User newUser(@RequestBody LinkedHashMap<String, String> userMap) {
		User currentUser = new User(userMap.get("userName"), userMap.get("password"), userMap.get("email"));
		
		if(uRepo.selectByUsername(currentUser.getUsername()) == null) {
			uRepo.insert(currentUser);
			return currentUser;
		}
		else {return null;}
	}
	
	@PostMapping(value = "/generatePasswordCode", consumes = "application/json", produces = "application/json")
	public void generatePasswordCode(@RequestBody LinkedHashMap<String, String> userMap) {
		User currentUser = uRepo.selectByEmail(userMap.get("email"));
		if (currentUser == null)
			return;
		String newPasswordCode = generateRandomAlphaNumericString(6,6);
		currentUser.setPasswordCode(newPasswordCode);
		currentUser.setPasswordCodeGeneratedOnDate(new Timestamp(System.currentTimeMillis()));
		uRepo.update(currentUser);
		eServ.sendEmail(userMap.get("email"), "My Media Database New Password Code", "Your new password code is " + newPasswordCode);
	}
	
	@PostMapping(value = "/changePassword", consumes = "application/json", produces = "application/json")
	public boolean changePassword(@RequestBody LinkedHashMap<String, String> userMap) {
		if (uRepo.isValidPasswordCode(userMap.get("email"), userMap.get("passwordCode"))) {
			User currentUser = uRepo.selectByEmail(userMap.get("email"));
			long fixMeImHardCodedInABadPlace = 1000 * 60 * 10;  //1000 ms, in a second, 60 seconds in a minute, 10 minutes, so if 10 minutes have passed
			if ((new Timestamp(System.currentTimeMillis())).getTime() - 
					currentUser.getPasswordCodeGeneratedOnDate().getTime() > fixMeImHardCodedInABadPlace)
				return false;
			currentUser.setPassword(userMap.get("newPassword"));
			uRepo.update(currentUser);
			return true;
		}
		return false;
	}

    private static String generateRandomAlphaNumericString(int min, int max) {
        int leftLimit = 48; // number 0
        int rightLimit = 122; // letter 'z'
        int targetStringLength = ThreadLocalRandom.current().nextInt(min, max + 1);
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
            .limit(targetStringLength)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();

        return generatedString;
    }
    
	private static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString().substring(0, 50);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

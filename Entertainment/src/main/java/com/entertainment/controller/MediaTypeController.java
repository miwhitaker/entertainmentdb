package com.entertainment.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entertainment.model.MediaType;
import com.entertainment.repository.MediaTypeRepository;


@RestController
@RequestMapping(path="/mediatypecontroller")
@CrossOrigin(origins="*")
public class MediaTypeController {
	private MediaTypeRepository mRepo;

	public MediaTypeController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public MediaTypeController(MediaTypeRepository mRepo) {
		super();
		this.mRepo = mRepo;
	}
	
	@GetMapping(value ="/init")
	public List<MediaType> insertAllTypes(){
		List<MediaType> types = new ArrayList<MediaType>(Arrays.asList(new MediaType("book"), new MediaType("movie"), new MediaType("music"), new MediaType("game")));
		for(MediaType type: types) {
			mRepo.insert(type);
		}
		return mRepo.selectAll();
	}

}

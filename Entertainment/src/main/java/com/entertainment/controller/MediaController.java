package com.entertainment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entertainment.model.Media;
import com.entertainment.service.MediaService;


@RestController
@RequestMapping(path="/media_search")
@CrossOrigin(origins="*")
public class MediaController {
	private MediaService mServ;

	public MediaController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public MediaController(MediaService mServ) {
		super();
		this.mServ = mServ;
	}
	
	@GetMapping(value ="/movie/{movieName}")
	public List<Media> movieSearch(@PathVariable("movieName") String movieName){
		List<Media> returnedList = mServ.searchMovie(movieName);
		return returnedList;
	}
	
	@GetMapping(value ="/music/{musicName}")
	public List<Media> musicSearch(@PathVariable("musicName") String musicName){
		List<Media> returnedList = mServ.searchMusic(musicName);
		return returnedList;
	}
	
	@GetMapping(value ="/game/{gameName}")
	public List<Media> gameSearch(@PathVariable("gameName") String gameName){
		List<Media> returnedList = mServ.searchGame(gameName);
		return returnedList;
	}
	
	@GetMapping(value ="/book/{bookName}")
	public List<Media> bookSearch(@PathVariable("bookName") String bookName){
		List<Media> returnedList = mServ.searchBook(bookName);
		return returnedList;
	}

}

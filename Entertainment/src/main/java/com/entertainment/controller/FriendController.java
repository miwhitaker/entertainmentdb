package com.entertainment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entertainment.model.Friend;
import com.entertainment.model.User;
import com.entertainment.service.FriendService;
import com.entertainment.service.UserService;

@RestController
@RequestMapping(path="/user")
@CrossOrigin(origins="*")
public class FriendController {
	
	private UserService uServ;
	private FriendService fServ;
	
	public FriendController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public FriendController(UserService uServ, FriendService fServ) {
		super();
		this.uServ = uServ;
		this.fServ = fServ;
	}
	
	@PostMapping("/{id}/add_friend")
	public Object addFriend(@RequestBody String username, @RequestBody @PathVariable("id")int id ){
		User user = uServ.getUserById(id);
		User friend = uServ.getUserByUsername(username);
		if(user.getUsername().equals(username))
			return null;
		if(friend == null)
			return null;
		if(fServ.isFriend(user, friend))
			return null;
		fServ.insertFriend(new Friend(user, friend));
		return friend;
	}
	
	@GetMapping("/get_all_friends")
	public ResponseEntity<Object> getFriend() {
		//todo(jordan): do we want this in friendcontroller or usercontroller?
		
		return new ResponseEntity<Object>(null);
	}
	
	//
	@GetMapping("/{id}/get_friends")
	public List<Friend> getUserFriends(@PathVariable("id") int id){
		User user = uServ.getUserById(id);
		List<Friend> friends = fServ.getAllFriendByUser(user);
		if(friends == null || friends.size() == 0) {
			return null;
		}
		return friends;
	}
	
	//select distinct ut.user_id, ut.username, ft.user_user_id, ft.friend_user_id from user_table ut left join friend_table ft on ut.user_id = ft.user_user_id where ut.username = 'zach';
	@GetMapping("/{username}/get_friends_by_username")
	public List<Friend> getUserFriends(@PathVariable("username") String username){
		//System.out.println("hitting this end of get friends");
		User user = uServ.getUserByUsername(username);
		//System.out.println("user is user"); 
		List<Friend> friends = fServ.getAllFriendByUser(user);
		//System.out.println("hello");
		if(friends == null || friends.size() == 0) {
			return null;
		}
		return friends;
	}
	
	
	
}

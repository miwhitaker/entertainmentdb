package com.entertainment.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.entertainment.model.Friend;
import com.entertainment.model.Review;
import com.entertainment.model.User;
import com.entertainment.repository.MediaTypeRepository;
import com.entertainment.service.FriendService;
import com.entertainment.service.ReviewService;
import com.entertainment.service.UserService;

@RestController
@RequestMapping(path="/user")
@CrossOrigin(origins="*")
public class ReviewController {
	private UserService uServ;
	private ReviewService rServ;
	private FriendService fServ;
	private MediaTypeRepository mRepo;
	
	public ReviewController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public ReviewController(UserService uServ, ReviewService rServ, FriendService fServ, MediaTypeRepository mRepo) {
		super();
		this.uServ = uServ;
		this.rServ = rServ;
		this.fServ = fServ;
		this.mRepo = mRepo;
	}
	
	@GetMapping(value="/{id}/get_all")
	public List<User> getAllExceptCurrent(@RequestBody @PathVariable("id") int id){
		User user = uServ.getUserById(id);
		return uServ.getAllExceptCurrent(user);
	}
	
	@GetMapping(value="/{id}/get_timeline")
	public Object getTimelineReviews(@PathVariable("id") int id){
		User user = uServ.getUserById(id);
		List<Friend> friends = fServ.getAllFriendByUser(user);
		List<Review> friendReviews = new ArrayList<>();
		if(friends != null && friends.size() != 0) {
			for(Friend friend: friends) {
				List<Review> reviews = rServ.getReviewsByFriendId(friend.getFriend().getUserId());
				if(reviews != null) {
					for(Review review: reviews) {
						friendReviews.add(review);
					}
				}
			}
		}
		List<Review> myReviews = rServ.getReviewsByUserId(id);
		if(myReviews != null)
			friendReviews.addAll(myReviews);
		if(friendReviews.size() == 0)
			return null;
		return friendReviews;
	}
	
	@PostMapping(value="/reviews/{searchText}")
	public List<Review> getReviewsBySearch(@PathVariable("searchText") String searchText){
		return rServ.getReviewsBySearch(searchText.toLowerCase());
	}
	
	@PostMapping(value="/{mediaType}/reviews/{searchText}")
	public List<Review> getReviewsBySearchByMediaType(@PathVariable("searchText") String searchText, @PathVariable("mediaType") String mediaType){
		return rServ.getReviewsBySearchByMediaType(searchText.toLowerCase(), mediaType);
	}

	//blame zach
	@GetMapping(value="/get_reviews/{username}") //example /get_reviews/400 gets reviews from user 400
	public ResponseEntity<Object> getUserReviews(@PathVariable("username") String username){
		List<Review> userReviews =  rServ.getReviewsByUserUsername(username);
		
		return new ResponseEntity<>(userReviews, HttpStatus.OK);
	}
	
	@PostMapping("/{id}/post_review")
	public Review insertReview(@RequestBody Review review, @RequestBody @PathVariable("id") int id){
		User user = uServ.getUserById(id);
		review.setUser(user);
		String type = review.getMediaType().getType().toLowerCase();
		review.setMediaType(mRepo.selectByType(type));
		rServ.addReview(review);
		return review;
	}
	
	@GetMapping(value="/get_media_type_reviews/{mediaType}")
	public ResponseEntity<Object> getReviewsByMediaType(@PathVariable("mediaType") String mediaType)
	{
		List<Review> typeReviews = rServ.getReviewsByMediaType(mediaType);
		
		return new ResponseEntity<>(typeReviews, HttpStatus.OK);
	}
	
	//blame zach
	@GetMapping(value="{userId}/get_my_freind_reviews")
	public ResponseEntity<Object> getFriendReviewsOfCurUser(@PathVariable("userId") int userId){
		List<Review> friendsReviews = rServ.getReviewsByFriends(userId);
		return new ResponseEntity<>(friendsReviews, HttpStatus.OK);
	}
}

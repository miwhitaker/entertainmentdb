package com.entertainment.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.annotation.MultipartConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.entertainment.model.ProfilePicture;
import com.entertainment.model.User;
import com.entertainment.repository.ProfilePictureRepository;
import com.entertainment.service.UserService;

@RestController
@RequestMapping(path="/user")
@CrossOrigin(origins="*")
@MultipartConfig
public class ProfilePictureController {
	private ProfilePictureRepository ppRepo;
	private UserService uServ;
	
	public ProfilePictureController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public ProfilePictureController(ProfilePictureRepository ppRepo, UserService uServ)
	{
		super();
		this.uServ = uServ; 
		this.ppRepo = ppRepo;
	}
	
	@PostMapping(value="/{userId}/submit_picture")
	public byte[] submitPicture(@RequestParam("uploadFile") MultipartFile image, @PathVariable("userId") int userId) throws IOException
	{
		User user = uServ.getUserById(userId);
		System.out.println(user);
		ProfilePicture picture = new ProfilePicture(image.getBytes(), user);
		System.out.println(picture);
		ppRepo.setImage(picture);
		return image.getBytes();
	}
	
	@GetMapping(value="/{username}/get_picture")
	public List<ProfilePicture> getProfilePicture(@PathVariable("username") String username)
	{
		System.out.println(username);
		return ppRepo.getImage(uServ.getUserByUsername(username));
	}
}

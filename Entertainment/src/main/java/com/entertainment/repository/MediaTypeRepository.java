package com.entertainment.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entertainment.model.MediaType;

@Transactional
@Repository("mediaTypeRepo")
public class MediaTypeRepository{
	private SessionFactory sessionFactory;

	public MediaTypeRepository() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public MediaTypeRepository(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}
	
	public List<MediaType> selectAll(){
		return sessionFactory.getCurrentSession().createQuery("from MediaType", MediaType.class).list();
	}

	public MediaType selectById(int id) {
		return sessionFactory.getCurrentSession().get(MediaType.class, id);
	}
	
	public MediaType selectByType(String type) {
		List<MediaType> types = sessionFactory.getCurrentSession().createQuery("from MediaType where type='"+type+"'", MediaType.class).list();
		if(types.size() == 0)
			return null;
		return types.get(0);
	}

	public void insert(MediaType type) {
		sessionFactory.getCurrentSession().save(type);
	}

	public void update(MediaType type) {
		sessionFactory.getCurrentSession().merge(type);
	}

	public void delete(MediaType type) {
		sessionFactory.getCurrentSession().delete(type);
	}

}

package com.entertainment.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entertainment.model.User;

@Transactional
@Repository("userRepo")
public class UserRepository{
	
	private SessionFactory sessionFactory;
	
	public UserRepository() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public UserRepository(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}
	
	public List<User> selectAll(){
		return sessionFactory.getCurrentSession().createQuery("from User order by username", User.class).list();
	}
	
	public User selectById(int id) {
		User u = sessionFactory.getCurrentSession().get(User.class, id);
		return u;
	}
	
	public User selectByUsername(String username) {
		List<User> users = sessionFactory.getCurrentSession().createQuery("from User where username='"+username+"'", User.class).list();
		if(users.size() == 0)
			return null;
		return users.get(0);
	}
	
	public User selectByEmail(String email) {
		List<User> users = sessionFactory.getCurrentSession().createQuery("from User where email='"+email+"'", User.class).list();
		if(users.size() == 0)
			return null;
		return users.get(0);
	}
	
	public boolean isValidPasswordCode(String email, String passwordCode) {
		List<User> users = sessionFactory.getCurrentSession().createQuery("from User where email='"+email+"'" 
	+ " and passwordCode='" + passwordCode + "'", User.class).list();
		return (users.size() == 0) ? false : true;
	}
	
	public void insert(User user) {
		sessionFactory.getCurrentSession().save(user);
	}
	
	public void update(User user) {
		sessionFactory.getCurrentSession().merge(user);
	}
	
	public void delete(User user) {
		sessionFactory.getCurrentSession().delete(user);
	}

}

package com.entertainment.repository;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entertainment.model.Friend;
import com.entertainment.model.User;


@Transactional
@Repository("friendRepo")
public class FriendRepository{
	private SessionFactory sessionFactory;

	public FriendRepository() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public FriendRepository(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}
	
	public List<Friend> selectAll() {
		return sessionFactory.getCurrentSession().createQuery("from Friend", Friend.class).list();
	}

	public Friend selectById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Friend> selectAllByUser(User user) {
		//System.out.println(user + " in friend repo");
		if (user == null) {
			return new LinkedList<Friend>();
		}
		return sessionFactory.getCurrentSession().createQuery("from Friend where user.userId="+user.getUserId(), Friend.class).list();
	}

	public void insert(Friend entity) { 
		sessionFactory.getCurrentSession().save(entity);	
	}

	public void update(Friend entity) {
		sessionFactory.getCurrentSession().merge(entity);
	}

	public void delete(Friend entity) {
		sessionFactory.getCurrentSession().delete(entity);
		
	}

}

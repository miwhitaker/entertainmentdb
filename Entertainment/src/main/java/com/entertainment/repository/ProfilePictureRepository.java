package com.entertainment.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entertainment.model.ProfilePicture;
import com.entertainment.model.User;

@Transactional
@Repository
public class ProfilePictureRepository {
	private SessionFactory sessionFactory;
	
	public ProfilePictureRepository() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public ProfilePictureRepository(SessionFactory sessionFactory)
	{
		super();
		this.sessionFactory = sessionFactory;
	}
	
	public void setImage(ProfilePicture image)
	{
		sessionFactory.getCurrentSession().saveOrUpdate(image);
	}
	
	public List<ProfilePicture> getImage(User user)
	{
		return sessionFactory.getCurrentSession().createQuery("from ProfilePicture p where user.userId=" + user.getUserId(), ProfilePicture.class).list();
	}
}

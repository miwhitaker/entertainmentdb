package com.entertainment.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entertainment.model.Review;
import com.entertainment.model.User;

@Transactional
@Repository("reviewRepo")
public class ReviewRepository{
	private SessionFactory sessionFactory;
	
	public ReviewRepository() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public ReviewRepository(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}
	
	public List<Review> selectAll(){
		return sessionFactory.getCurrentSession().createQuery("from Review", Review.class).list();
	}
	
	public List<Review> selectAllByUser(User user){
		return sessionFactory.getCurrentSession().createQuery("from Review where user="+user+" order by postedDate", Review.class).list();
	}
	
	public List<Review> selectAllByFriendId(int id){
		return sessionFactory.getCurrentSession().createQuery("from Review as r left join fetch r.mediaType left join fetch r.user as u where u.userId="+id +"order by r.postedDate desc", Review.class).list();
	}
	
	public List<Review> selectAllByUserId(int id){
		return sessionFactory.getCurrentSession().createQuery("from Review where user.userId="+id+"order by postedDate desc", Review.class).list();
	}
	
	//blame Zach
	public List<Review> selectAllByUsername(String name){
		return sessionFactory.getCurrentSession().createNativeQuery(
				"select rt.review_id, "
					+ "rt.description, "
					+ "rt.media_image, "
					+ "rt.media_title, "
					+ "rt.posted_date, "
					+ "rt.rating, "
					+ "rt.type_id, "
					+ "rt.user_fk "
				+ "from review_table rt "
				+ "left join user_table ut "
				+ "on ut.user_id = rt.user_fk "
				+ "where ut.username = '"+name+"'"
				//"from Review as r left join fetch r.user.username = "+name+" order by postedDate desc", Review.class).list();
				, Review.class).list();
				
	}
	
	public List<Review> selectAllByTitle(String title) {
		return sessionFactory.getCurrentSession().createQuery("from Review where mediaTitle LIKE '%"+title+"%' order by postedDate desc", Review.class).list();
	}
	
	public List<Review> selectAllBySearchTextByMedia(String searchText, String mediaType) {
		return sessionFactory.getCurrentSession().createQuery("from Review as r left join fetch r.mediaType left join fetch r.user as u where r.mediaType.type ='"+mediaType+"' and (lower(r.mediaTitle) LIKE '%"+searchText+"%'  or lower(r.user.username) LIKE '%"+searchText+"%')  order by r.postedDate desc", Review.class).list();
	}
	
	public List<Review> selectAllBySearchText(String searchText) {
		return sessionFactory.getCurrentSession().createQuery("from Review as r left join fetch r.mediaType left join fetch r.user as u where lower(r.mediaTitle) LIKE '%"+searchText+"%' or lower(r.user.username) LIKE '%"+searchText+"%' order by r.postedDate desc", Review.class).list();
	}
	
	public List<Review> selectAllByMediaType(String mediaType)
	{
		return sessionFactory.getCurrentSession().createQuery("from Review where mediaType.type='"+mediaType+"'", Review.class).list();
	}
	
	
	//Blame Zach
	public List<Review> selectAllByUserFriendId(int id){
		//This is the query that selects reviews that friends associated with a given user have posted
			//select rt.review_id, rt.description, rt.media_image, rt.media_title, rt.posted_date, rt.rating, rt.type_id, rt.user_fk from review_table rt left join friend_table ft on rt.user_fk = ft.friend_user_id where ft.user_user_id = 1;
		//Alternate query
			//"from Review as r left join fetch r.mediaType left join fetch r.user as u where u.userId="+id +"order by r.postedDate"
			//"from Review as r left join fetch r.mediaType left join fetch r.user as u where f.friendId"+id +"order by r.postedDate"
		// from Review as r left join fetch friend_table ft on rt.user_fk = ft.friend_user_id where ft.friend_user_id = 1 order by postedDate desc;
		//select distinct rt.description, rt.media_image, rt.media_title, rt.posted_date, rt.rating, rt.type_id, rt.user_fk 
		//from review_table rt 
		//left join friend_table ft 
		//on rt.user_fk = ft.friend_user_id
		/**
		 *		select distinct 
				 * 	rt.description, 
				 * 	rt.media_title, 
				 * 	rt.media_image, 
				 * 	rt.posted_date, 
				 * 	rt.rating, 
				 * 	rt.type_id, 
				 * 	rt.user_fk 
		 * 		from Review rt 
		 * 		left join re.user as u
		 * 		left join u. friend as ft
		 *  	on rt.user_fk = ft.friend_user_id 
		 *		where ft.friend_user_id = 4; 
		 *
		 */
		//Native SQL
		return sessionFactory.getCurrentSession().createNativeQuery("select distinct rt.review_id, "
																		+ "rt.description, "
																		+ "rt.media_image, "
																		+ "rt.media_title, "
																		+ "rt.posted_date, "
																		+ "rt.rating, "
																		+ "rt.type_id, "
																		+ "rt.user_fk "
																	+ "from review_table rt "
																	+ "left join friend_table ft "
																	+ "on rt.user_fk = ft.friend_user_id "
																	+ "where ft.friend_user_id = '"+id+"'",
																	Review.class).list();
		
		//where ft.friend_user_id = ; 
		/*return sessionFactory.getCurrentSession().createQuery(	  "select distinct r.description, r.mediaImage, r.mediaTitle, r.postedDate, r.rating, r.mediaType, r.user "
																+ "from Review as r "						//from
																+ "left join fetch Friend as f "			//left join
																+ "on r.user = f.friend_user_id "			//on
																+ "where f.friend_user_id = " + id + " "	//where id =
																+ "order by postedDate desc"				//order by
																+ "", Review.class).list();
		*/
		//I'm not completely sure how this maps to Hibernate syntax 
	}
	 
	public Review selectById(int id) {
		return sessionFactory.getCurrentSession().get(Review.class, id);
	}
	
	public void insert(Review review) {
		sessionFactory.getCurrentSession().save(review);
	}
	
	public void update(Review review) {
		sessionFactory.getCurrentSession().merge(review);
	}
	
	public void delete(Review review) {
		sessionFactory.getCurrentSession().delete(review);
	}
}

package com.entertainment.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="profile_pictures")
public class ProfilePicture implements Serializable{
	
	@Id
	@OneToOne
	private User user;
	
	@Column(name="image")
	private byte[] image;
	
	
	
	public ProfilePicture() {
		// TODO Auto-generated constructor stub
	}
	
	public ProfilePicture(byte[] image, User user)
	{
		this.image = image;
		this.user = user;
	}

	@Override
	public String toString() {
		return "ProfilePicture [user=" + user + "]";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
}

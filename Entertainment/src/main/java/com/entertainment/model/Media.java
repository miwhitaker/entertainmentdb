package com.entertainment.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="media_table")
public class Media { //turn this abstract if adding every type?
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int mediaId;
	
	@Column(name="title", nullable=false)
	private String title;

	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private MediaType type;
	
	@Column(name="image")
	private String image;
	
	public Media() {
		super();
	}

	public Media(String title, MediaType type, String image) {
		super();
		this.title = title;
		this.type = type;
		this.image = image;
	}

	@Override
	public String toString() {
		return "Media [mediaId=" + mediaId + ", title=" + title + ", type=" + type + ", image=" + image + "]";
	}

	public int getMediaId() {
		return mediaId;
	}

	public void setMediaId(int mediaId) {
		this.mediaId = mediaId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MediaType getType() {
		return type;
	}

	public void setType(MediaType type) {
		this.type = type;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}


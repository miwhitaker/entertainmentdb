package com.entertainment.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="review_table")
public class Review {
	@Id
	@Column(name="review_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int reviewId;
	
	@Column(name="description")
	private String description;
	
	@Column(name="media_title", nullable=false)
	private String mediaTitle;
	
	@Column(name="media_image", nullable=false)
	private String mediaImage;
	
	@Column(name="posted_date")
	private Timestamp postedDate;
	
	@Column(name="rating")
	private double rating;
	
	//look up table
	//changed fetchtype of type_id and user_fk to eager because i got an error, below link for more info
	//https://stackoverflow.com/questions/26957554/jsonmappingexception-could-not-initialize-proxy-no-session
    @ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER) 
    @JoinColumn(name = "type_id")
	private MediaType mediaType;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="user_fk")
	private User user;

	public Review() {
		// TODO Auto-generated constructor stub
	}

	public Review(int reviewId, String description, String mediaTitle, String mediaImage, Timestamp postedDate, double rating,
			MediaType mediaType, User user) {
		super();
		this.reviewId = reviewId;
		this.description = description;
		this.mediaTitle = mediaTitle;
		this.mediaImage = mediaImage;
		this.postedDate = postedDate;
		this.rating = rating;
		this.mediaType = mediaType;
		this.user = user;
	}

	public Review(String description, String mediaTitle, String mediaImage, Timestamp postedDate, double rating,
			MediaType mediaType, User user) {
		super();
		this.description = description;
		this.mediaTitle = mediaTitle;
		this.mediaImage = mediaImage;
		this.postedDate = postedDate;
		this.rating = rating;
		this.mediaType = mediaType;
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMediaTitle() {
		return mediaTitle;
	}

	public void setMediaTitle(String mediaTitle) {
		this.mediaTitle = mediaTitle;
	}

	public String getMediaImage() {
		return mediaImage;
	}

	public void setMediaImage(String mediaImage) {
		this.mediaImage = mediaImage;
	}

	public Timestamp getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(Timestamp postedDate) {
		this.postedDate = postedDate;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getReviewId() {
		return reviewId;
	}

	@Override
	public String toString() {
		return "Review [reviewId=" + reviewId + ", description=" + description + ", mediaTitle=" + mediaTitle
				+ ", mediaImage=" + mediaImage + ", postedDate=" + postedDate + ", rating=" + rating + ", mediaType="
				+ mediaType + ", user=" + user + "]";
	}
}

package com.entertainment.model;

import java.security.MessageDigest;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_table")
@Embeddable
public class User {
	@Id
	@Column(name="user_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userId;
	
	@Column(name="username", unique=true, nullable=false)
	private String username;
	
	@Column(name="password", nullable=false)
//	@ColumnTransformer(read="", write="") //for encryption
	private String password;
	
	@Column(name="email", nullable=false)
	private String email;
	
	@Column(name="passwordCode", nullable=true)
	private String passwordCode;
	
	@Column(name="passwordCodeGeneratedOnDate", nullable=true)
	private Timestamp passwordCodeGeneratedOnDate;
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(String username, String password, String email) {
		super();
		this.username = username;
		this.password = sha256(password);
		this.email = email;
	}

	public User(int userId, String username, String password, String email) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = sha256(password);
		this.email = email;
	}

	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = sha256(password);
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	public int getUserId() {
		return userId;
	}

	public String getPasswordCode() {
		return passwordCode;
	}

	public void setPasswordCode(String passwordCode) {
		this.passwordCode = passwordCode;
	}

	public Timestamp getPasswordCodeGeneratedOnDate() {
		return passwordCodeGeneratedOnDate;
	}

	public void setPasswordCodeGeneratedOnDate(Timestamp passwordCodeGeneratedOnDate) {
		this.passwordCodeGeneratedOnDate = passwordCodeGeneratedOnDate;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", passwordCode=" + passwordCode + ", passwordCodeGeneratedOnDate=" + passwordCodeGeneratedOnDate
				+ "]";
	}
	
	private static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString().substring(0, 50);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

package com.entertainment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="media_type_table")
public class MediaType {
	@Id
	@Column(name="type_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int typeId;
	
	@Column(name="type", unique=true, nullable=false)
	private String type;
	
	public MediaType() {
		// TODO Auto-generated constructor stub
	}

	public MediaType(String type) {
		super();
		this.type = type;
	}

	public MediaType(int id, String type) {
		super();
		this.typeId = id;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return typeId;
	}

	@Override
	public String toString() {
		return "MediaType [id=" + typeId + ", type=" + type + "]";
	}
}

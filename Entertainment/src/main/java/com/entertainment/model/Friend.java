package com.entertainment.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="friend_table")
public class Friend {
	@Id
	@Column(name="friend_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int friendId;

	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private User user;

	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private User friend;

	public Friend() {
		// TODO Auto-generated constructor stub
	}

	public Friend(User user, User friend) {
		super();
		this.user = user;
		this.friend = friend;
	}

	public Friend(int friendId, User user, User friend) {
		super();
		this.friendId = friendId;
		this.user = user;
		this.friend = friend;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getFriend() {
		return friend;
	}

	public void setFriend(User friend) {
		this.friend = friend;
	}

	public int getFriendId() {
		return friendId;
	}

	@Override
	public String toString() {
		return "Friend [friendId=" + friendId + ", user=" + user + ", friend=" + friend + "]";
	}
	
	
}

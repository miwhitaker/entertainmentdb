package com.entertainment.test;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.security.MessageDigest;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.entertainment.controller.LoginController;
import com.entertainment.model.User;
import com.entertainment.repository.UserRepository;
import com.entertainment.service.EmailService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes={ LoginController.class })
@EnableWebMvc
public class LoginTestController {
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private UserRepository uRepo;
	
	@MockBean
	private EmailService eServ;
	
	private User testUser = new User(1, "testUser", "password", "test@email.com");
	private User testUser1 = new User(2, "testUser1", "password1", "test1@email.com");
	Map<String, String> input = new LinkedHashMap<>();
	
	@BeforeEach
	public void setUp() {
		this.mockMvc = webAppContextSetup(context).build();
		input.put("userName", "testUser");
		input.put("password", "password");
		input.put("email", "test@email.com");
	}
	
	@Test
	public void validLogin() throws Exception {
		when(uRepo.selectByUsername("testUser")).thenReturn(testUser);
		mockMvc.perform(MockMvcRequestBuilders.post("/logincontroller/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(input))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.userId").value(1))
				.andExpect(jsonPath("$.username").value("testUser"))
				.andExpect(jsonPath("$.password").value(sha256("password")))
				.andExpect(jsonPath("$.email").value("test@email.com"));
	}
	
	@Test
	public void inValidLogin() throws Exception {
		when(uRepo.selectByUsername("testUser")).thenReturn(testUser1);
		mockMvc.perform(MockMvcRequestBuilders.post("/logincontroller/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(input))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());
	}
	
	@Test
	public void validRegistration() throws Exception {
		when(uRepo.selectByUsername("testUser")).thenReturn(null);
		mockMvc.perform(MockMvcRequestBuilders.post("/logincontroller/register")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(input))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.username").value("testUser"))
				.andExpect(jsonPath("$.password").value(sha256("password")))
				.andExpect(jsonPath("$.email").value("test@email.com"));
	}
	
	@Test
	public void inValidRegistration() throws Exception {
		when(uRepo.selectByUsername("testUser")).thenReturn(testUser);
		mockMvc.perform(MockMvcRequestBuilders.post("/logincontroller/register")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(input))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	      return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	      throw new RuntimeException(e);
	    }
	}

	private static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString().substring(0, 50);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

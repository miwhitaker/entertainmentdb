package com.entertainment.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.entertainment.controller.MediaController;
import com.entertainment.model.Media;
import com.entertainment.model.MediaType;
import com.entertainment.service.MediaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes={ MediaController.class })
@EnableWebMvc
public class MediaTestController {
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private MediaService mServ;
	
	private List<Media> testBookList;
	private List<Media> testGameList;
	private List<Media> testMovieList;
	private List<Media> testMusicList;

	private Media testMediaBook1 = new Media("testTitle1", new MediaType("Book"), "testImageUrl1");
	private Media testMediaBook2 = new Media("testTitle1", new MediaType("Book"), "testImageUrl2");
	private Media testMediaGame1 = new Media("testTitle2", new MediaType("Game"), "testImageUrl3");
	private Media testMediaGame2 = new Media("testTitle2", new MediaType("Game"), "testImageUrl4");
	private Media testMediaMovie1 = new Media("testTitle3", new MediaType("Movie"), "testImageUrl5");
	private Media testMediaMovie2 = new Media("testTitle3", new MediaType("Movie"), "testImageUrl6");
	private Media testMediaMusic1 = new Media("testTitle4", new MediaType("Music"), "testImageUrl7");
	private Media testMediaMusic2 = new Media("testTitle4", new MediaType("Music"), "testImageUrl8");
	
	@BeforeEach
	public void setUp() {
		this.mockMvc = webAppContextSetup(context).build();
		testBookList = Arrays.asList(testMediaBook1, testMediaBook2);
		testGameList = Arrays.asList(testMediaGame1, testMediaGame2);
		testMovieList = Arrays.asList(testMediaMovie1, testMediaMovie2);
		testMusicList = Arrays.asList(testMediaMusic1, testMediaMusic2);
		when(mServ.searchBook("testTitle1")).thenReturn(testBookList);
		when(mServ.searchGame("testTitle2")).thenReturn(testGameList);
		when(mServ.searchMovie("testTitle3")).thenReturn(testMovieList);
		when(mServ.searchMusic("testTitle4")).thenReturn(testMusicList);
	}
	
	@Test 
	public void validSearchBook() throws Exception{
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/media_search/book/{bookName}", "testTitle1")
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

        assertEquals(asJsonString(testBookList),result.getResponse().getContentAsString());

	}
	
	@Test 
	public void validSearchGame() throws Exception{
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/media_search//game/{gameName}", "testTitle2")
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

        assertEquals(asJsonString(testGameList),result.getResponse().getContentAsString());

	}
	
	@Test 
	public void validSearchMovie() throws Exception{
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/media_search/movie/{movieName}", "testTitle3")
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

        assertEquals(asJsonString(testMovieList),result.getResponse().getContentAsString());

	}
	
	@Test 
	public void validSearchMusic() throws Exception{
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/media_search/music/{musicName}", "testTitle4")
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

        assertEquals(asJsonString(testMusicList),result.getResponse().getContentAsString());

	}
	
	public static String asJsonString(final Object obj) {
	    try {
	      return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	      throw new RuntimeException(e);
	    }
	}
}

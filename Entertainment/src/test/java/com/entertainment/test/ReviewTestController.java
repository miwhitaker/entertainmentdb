package com.entertainment.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.entertainment.controller.ReviewController;
import com.entertainment.model.Friend;
import com.entertainment.model.Review;
import com.entertainment.model.User;
import com.entertainment.model.MediaType;
import com.entertainment.repository.MediaTypeRepository;
import com.entertainment.service.FriendService;
import com.entertainment.service.ReviewService;
import com.entertainment.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes={ ReviewController.class })
@EnableWebMvc
public class ReviewTestController {
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private UserService uServ;
	
	@MockBean
	private FriendService fServ;

	@MockBean
	private ReviewService rServ;

	@MockBean
	private MediaTypeRepository mRepo;

	private User testUser1 = new User(1, "testUser1", "password", "test1@email.com");
	private User testUser2 = new User(2, "testUser2", "password", "test2@email.com");
	private User testUser3 = new User(3, "testUser3", "password", "test3@email.com");
	private User testUser4 = new User(4, "testUser4", "password", "test4@email.com");
	
	private List<User> allExceptUserList = new ArrayList<User>(Arrays.asList(testUser2, testUser3, testUser4));
	
	private Friend friend1 = new Friend(testUser1, testUser2);
	private Friend friend2 = new Friend(testUser1, testUser3);
	
	private List<Friend> friendList = new ArrayList<Friend>(Arrays.asList(friend1, friend2));
	
	private Review myReview1 = new Review("my description1", "my title1", "my imageURL1", new Timestamp(0), 3.8, new MediaType("Book"), testUser1);
	private Review myReview2 = new Review("my description2", "my title2", "my imageURL2", new Timestamp(0), 2.2, new MediaType("Movie"), testUser1);
	private List<Review> myReviewList = new ArrayList<>(Arrays.asList(myReview1, myReview2));
	
	private Review friendReview1 = new Review("description1", "title1", "imageURL1", new Timestamp(0), 4.3, new MediaType("Book"), testUser2);
	private Review friendReview2 = new Review("description2", "title2", "imageURL2", new Timestamp(0), 2.1, new MediaType("Movie"), testUser2);
	private Review friendReview3 = new Review("description3", "title3", "imageURL3", new Timestamp(0), 5, new MediaType("Game"), testUser3);
	private Review friendReview4 = new Review("description4", "title4", "imageURL4", new Timestamp(0), 1.8, new MediaType("Music"), testUser3);
	private List<Review> friend2ReviewList = new ArrayList<>(Arrays.asList(friendReview1, friendReview2));
	private List<Review> friend3ReviewList = new ArrayList<>(Arrays.asList(friendReview3, friendReview4));
	
	List<Review> timelineReviews = new ArrayList<>();
	
	String searchText = "someSearchTextForTesting";
	
	@BeforeEach
	public void setUp() {
		this.mockMvc = webAppContextSetup(context).build();
		when(uServ.getUserById(1)).thenReturn(testUser1);
		when(uServ.getAllExceptCurrent(testUser1)).thenReturn(allExceptUserList);
		when(fServ.getAllFriendByUser(testUser1)).thenReturn(friendList);
		when(rServ.getReviewsByUserId(1)).thenReturn(myReviewList);
		
		timelineReviews.addAll(friend2ReviewList);
		timelineReviews.addAll(friend3ReviewList);
		timelineReviews.addAll(myReviewList);
		
		when(rServ.getReviewsBySearch(searchText.toLowerCase())).thenReturn(myReviewList);
		when(rServ.getReviewsBySearchByMediaType(searchText.toLowerCase(), "testMediaType")).thenReturn(myReviewList);
	}
	
	@Test
	public void validGetAllUsersExceptCurrent() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}/get_all", 1)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.content(asJsonString(1))
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		assertEquals(asJsonString(allExceptUserList),result.getResponse().getContentAsString());
	}
	
	@Test
	public void validGetReviewsForTimeline() throws Exception {
		when(fServ.getAllFriendByUser(testUser1)).thenReturn(friendList);
		when(rServ.getReviewsByFriendId(2)).thenReturn(friend2ReviewList);
		when(rServ.getReviewsByFriendId(3)).thenReturn(friend3ReviewList);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}/get_timeline", 1)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.content(asJsonString(1))
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		assertEquals(asJsonString(timelineReviews),result.getResponse().getContentAsString());
	}

	/*
	@Test
	public void validNoFriendsForTimeline() throws Exception {
		when(fServ.getAllFriendByUser(testUser1)).thenReturn(null);
		mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}/get_timeline", 1)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.content(asJsonString(1))
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());
	}
	*/
	
	@Test
	public void validNoReviewsForTimeline() throws Exception {
		when(fServ.getAllFriendByUser(testUser1)).thenReturn(friendList);
		when(rServ.getReviewsByFriendId(2)).thenReturn(null);
		when(rServ.getReviewsByFriendId(3)).thenReturn(null);
		when(rServ.getReviewsByUserId(1)).thenReturn(null);
		mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}/get_timeline", 1)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.content(asJsonString(1))
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());
	}
	
	@Test
	public void validSearch() throws Exception{
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/reviews/{searchText}", searchText)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		assertEquals(asJsonString(myReviewList),result.getResponse().getContentAsString());
	}
	
	@Test
	public void validSearchByMedia() throws Exception{
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/{mediaType}/reviews/{searchText}", "testMediaType", searchText)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		assertEquals(asJsonString(myReviewList),result.getResponse().getContentAsString());
	}
	
	/*
	@Test
	public void validUserReviews() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/user/get_reviews/{userid}", 1)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.content(asJsonString(1))
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		assertEquals(asJsonString(myReviewList),result.getResponse().getContentAsString());
	}
	
	@Test
	public void validUserNoReviews() throws Exception {
		when(rServ.getReviewsByUserId(1)).thenReturn(null);
		mockMvc.perform(MockMvcRequestBuilders.get("/user/get_reviews/{userid}", 1)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.content(asJsonString(1))
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());
	}
	*/
	
	@Test
	public void validNewReview() throws Exception{
		when(mRepo.selectByType("book")).thenReturn(new MediaType("Book"));
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/user/{id}/post_review", 1)
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.content(asJsonString(myReview1))
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
		assertEquals(asJsonString(myReview1),result.getResponse().getContentAsString());
	}
	
	@Test
	public void validGetReviewsByMedia() throws Exception{
		when(rServ.getReviewsByMediaType("book")).thenReturn(myReviewList);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/user/get_media_type_reviews/{mediaType}", "book")
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
		assertEquals(asJsonString(myReviewList),result.getResponse().getContentAsString());
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	      return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	      throw new RuntimeException(e);
	    }
	}
}

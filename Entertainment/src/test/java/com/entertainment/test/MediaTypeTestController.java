package com.entertainment.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.entertainment.controller.MediaTypeController;
import com.entertainment.model.MediaType;
import com.entertainment.repository.MediaTypeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes={ MediaTypeController.class })
@EnableWebMvc
public class MediaTypeTestController {
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private MediaTypeRepository mRepo;
	
	private List<MediaType> typeList = new ArrayList<MediaType>(Arrays.asList(new MediaType("book"), new MediaType("movie"), new MediaType("music"), new MediaType("game")));;
	
	@BeforeEach
	public void setUp() {
		this.mockMvc = webAppContextSetup(context).build();
		when(mRepo.selectAll()).thenReturn(typeList);
	}
	
	@Test
	public void validInitializeMediaTypes() throws Exception{
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/mediatypecontroller/init")
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

        assertEquals(result.getResponse().getContentAsString(),asJsonString(typeList));

	}
	
	public static String asJsonString(final Object obj) {
	    try {
	      return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	      throw new RuntimeException(e);
	    }
	}

}

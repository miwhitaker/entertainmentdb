package com.entertainment.test;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.entertainment.controller.FriendController;
import com.entertainment.model.User;
import com.entertainment.service.FriendService;
import com.entertainment.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes={ FriendController.class })
@EnableWebMvc
public class FriendTestController {
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private UserService uServ;
	
	@MockBean
	private FriendService fServ;
	
	private User testUser = new User(1, "testUser", "password", "test@email.com");
	private User friendUser = new User(2, "friendUser", "password", "friend@email.com");
	private Map<String, String> input = new HashMap<>();
	
	@BeforeEach
	public void setUp() {
		this.mockMvc = webAppContextSetup(context).build();
		input.put("username", "friendUser");
		input.put("id", "1");
		when(uServ.getUserById(1)).thenReturn(testUser);
		when(uServ.getUserByUsername(asJsonString(input))).thenReturn(friendUser);
	}
	
	@Test
	public void validAddNewFriend() throws Exception {
		when(fServ.isFriend(testUser, friendUser)).thenReturn(false);

		mockMvc.perform(MockMvcRequestBuilders.post("/user/{id}/add_friend", 1)
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(input))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.userId").value(2))
				.andExpect(jsonPath("$.username").value("friendUser"))
				.andExpect(jsonPath("$.password").value(sha256("password")))
				.andExpect(jsonPath("$.email").value("friend@email.com"));
	}
	
	@Test
	public void validAddAlreadyAddedFriend() throws Exception {
		when(fServ.isFriend(testUser, friendUser)).thenReturn(true);
		mockMvc.perform(MockMvcRequestBuilders.post("/user/{id}/add_friend", 1)
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(input))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());
	}
	
	@Test
	public void validAddNotExistingFriend() throws Exception {
		input.put("username", "nonExistingUser");
		when(fServ.isFriend(testUser, friendUser)).thenReturn(false);
		mockMvc.perform(MockMvcRequestBuilders.post("/user/{id}/add_friend", 1)
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(input))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").doesNotExist());
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	      return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	      throw new RuntimeException(e);
	    }
	}

	private static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString().substring(0, 50);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
